package gob

import (
	"os"
	"testing"

	"github.com/stretchr/testify/suite"
)

type TestFolderHierarchy struct {
	FolderName  string
	ChildHashes []string
}

type TestFile struct {
	Filename string
}

type MySuite struct {
	suite.Suite
	scmExisted bool
}

func (s *MySuite) SetupTest() {
	if _, err := os.Lstat(".scm"); !os.IsNotExist(err) {
		s.scmExisted = true
		s.FailNow(".scm exists already")
	} else {
		s.scmExisted = false
	}
}

func (s *MySuite) TearDownTest() {
	if !s.scmExisted {
		os.RemoveAll(".scm")
	}
}

func TestExampleTestSuite(t *testing.T) {
	suite.Run(t, new(MySuite))
}

func (s *MySuite) TestBasicStoreGet() {
	hash := Store([]byte("data"))

	result := Get(hash)

	s.Equal(result, []byte("data"))
}

func (s *MySuite) TestStoreFile() {
	f := TestFile{
		Filename: "filename",
	}
	Register(TestFile{})
	hash := Store(f)

	results := Get(hash)

	s.Equal(f, results)
}

func (s *MySuite) TestStoreFolderHierarchyOneFile() {
	f := TestFolderHierarchy{
		FolderName: "folder",
		ChildHashes: []string{
			"child-hash-1",
		},
	}
	Register(TestFolderHierarchy{})
	hash := Store(f)

	results := Get(hash)

	s.Equal(f, results)
}

func (s *MySuite) TestGetTypeDetectable() {
	fi := TestFile{
		Filename: "filename",
	}
	fo := TestFolderHierarchy{
		FolderName: "folder",
		ChildHashes: []string{
			"child-hash-1",
		},
	}

	Register(TestFile{})
	Register(TestFolderHierarchy{})

	hashi := Store(fi)
	hasho := Store(fo)

	resulti := Get(hashi)
	resulto := Get(hasho)

	fi_out := resulti.(TestFile)
	fo_out := resulto.(TestFolderHierarchy)

	s.NotNil(fi_out)
	s.NotNil(fo_out)
}

func (s *MySuite) TestStoreGetMap() {
	toStore := make(map[string]string)
	toStore["key"] = "value"
	toStore["key with spaces"] = "value with spaces"

	Register(map[string]string{})

	hash := Store(toStore)
	result := Get(hash).(map[string]string)

	s.Equal("value", result["key"])
	s.Equal("value with spaces", result["key with spaces"])
	s.Equal(2, len(result))
}

func (s *MySuite) TestStoreGetMapWithFilename() {
	toStore := make(map[string]string)
	toStore["key"] = "value"
	toStore["key with spaces"] = "value with spaces"

	Register(map[string]string{})

	StoreToFilename(".scm/testfile", toStore)
	result := GetFromFilename(".scm/testfile").(map[string]string)

	s.Equal("value", result["key"])
	s.Equal("value with spaces", result["key with spaces"])
	s.Equal(2, len(result))
}
