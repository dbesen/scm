package gob

import (
	"bytes"
	"encoding/gob"
	"io/ioutil"
	"os"
	path_pkg "path"
	"path/filepath"

	"bitbucket.org/dbesen/scm/internal/filesystem"
	"bitbucket.org/dbesen/scm/internal/hash"
)

func Register(value interface{}) {
	gob.Register(value)
}

func Store(f interface{}) string {
	// Gob-encode the data.  Any un-exported fields in the input will be ignored by gob.
	var network bytes.Buffer
	enc := gob.NewEncoder(&network)
	err := enc.Encode(&f)
	if err != nil {
		panic(err)
	}
	bytes := network.Bytes()

	// Sum the data
	hash := hash.SumData(bytes)

	// Write the data
	hash.EnsureHashFolder()

	mode := os.FileMode(0666)
	err = ioutil.WriteFile(path_pkg.Join(".scm", hash.Path(true)), bytes, mode)
	if err != nil {
		panic(err)
	}

	return hash.String()
}

func StoreToFilename(filename string, f interface{}) { // TODO dup'd
	// Gob-encode the data.  Any un-exported fields in the input will be ignored by gob.
	var network bytes.Buffer
	enc := gob.NewEncoder(&network)
	err := enc.Encode(&f)
	if err != nil {
		panic(err)
	}
	bytes := network.Bytes()

	filesystem.EnsureFolder(filepath.Dir(filename))
	mode := os.FileMode(0666)
	err = ioutil.WriteFile(filename, bytes, mode)
	if err != nil {
		panic(err)
	}
}

func GetFromFilename(filename string) interface{} { // TODO dup'd
	// Read data
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	// Gob decode
	network := bytes.NewBuffer(data)
	dec := gob.NewDecoder(network)
	var f interface{}
	err = dec.Decode(&f)
	if err != nil {
		panic(err)
	}

	return f
}

func Get(hash string) interface{} {
	// Read data
	data, err := ioutil.ReadFile(path_pkg.Join(".scm", hash))
	if err != nil {
		panic(err)
	}
	if len(data) == 0 {
		panic("Zero-length data loaded from " + hash)
	}

	// Gob decode
	network := bytes.NewBuffer(data)
	dec := gob.NewDecoder(network)
	var f interface{}
	err = dec.Decode(&f)
	if err != nil {
		panic(err)
	}

	return f
}
