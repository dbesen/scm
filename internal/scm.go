package scm

import (
	"fmt"
	"os"
	"strings"

	"bitbucket.org/dbesen/scm/internal/object_store"
)

func Main() {
	arg := os.Args[1]

	switch arg {
	case "save":
		Save(os.Args[2:], true)
	case "load":
		Load(os.Args[2:])
	case "parent":
		Parent(os.Args[2:])
	case "children":
		Children(os.Args[2:])
	case "show":
		Show(os.Args[2:])
	case "name":
		Name(os.Args[2:])
	case "names":
		Names(os.Args[2:])
	case "parents":
		Parents(os.Args[2:])
	default:
		fmt.Printf("Unknown command \"%s\"", arg)
		fmt.Println()
	}
}

func autosave() {
	Save([]string{}, false)
}

func Save(args []string, print bool) {
	var name string
	var hash string
	if len(args) == 1 {
		name = args[0]
		hash = object_store.StoreCWDNamed(name)
	} else {
		hash = object_store.StoreCWD()
	}
	if print {
		fmt.Println(hash)
	}
}

func Load(args []string) {
	var hash string

	if len(args) == 1 {
		hash = args[0]
	} else {
		hash = object_store.GetCWDHash()
	}

	autosave()

	object_store.RestoreCWDFromHashOrName(hash)
}

func Parent(args []string) {
	var hash string

	autosave()

	if len(args) == 1 {
		hash = object_store.GetParentHashOrNameOf(args[0])
	} else {
		hash = object_store.GetParentHashOrName()
	}

	fmt.Println(hash)
}

func Children(args []string) {
	var hashes []string
	autosave()

	if len(args) == 1 {
		hashes = object_store.GetChildrenForHashOrName(args[0])
	} else {
		hashes = object_store.GetChildren()
	}

	for _, hash := range hashes {
		fmt.Println(hash)
	}
}

func Show(args []string) {
	autosave()
	top_level_children := object_store.GetChildrenForRoot()
	for _, child := range top_level_children {
		printTreeRecursive(-1, "", child)
	}
}

func printTreeRecursive(level int, prefix string, node string) {
	if level > 0 {
		fmt.Print(strings.Repeat("   ", level))
	}
	fmt.Print(prefix)
	if object_store.GetNameIfExists(node) == object_store.GetCWDHashOrName() {
		fmt.Print("* ")
	}
	fmt.Println(node)
	children := object_store.GetChildrenForHashOrName(node)
	n := len(children)
	for i, child := range children {
		if i == n-1 {
			printTreeRecursive(level+1, "└─ ", child)
		} else {
			printTreeRecursive(level+1, "├─ ", child)
		}
	}
}

func Name(args []string) {
	if len(args) != 2 {
		fmt.Println("Usage: name <name> <hash>")
		os.Exit(1)
	}
	name := args[0]
	hash := args[1]
	// TODO verify hash is valid
	// TODO verify name isn't already taken? Or just output the old one?
	// TODO test renaming
	object_store.NameHash(name, hash)
}

func Names(args []string) {
	names := object_store.GetHashToNameMap()
	for hash, name := range names {
		fmt.Printf("%v: %v\n", name, hash)
	}
}

func Parents(args []string) {
	parents := object_store.GetChildToParentMap()
	for child, parent := range parents {
		fmt.Printf("%v is a child of %v\n", child, parent)
	}
}
