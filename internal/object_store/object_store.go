package object_store

import (
	"io"
	"os"
	path_pkg "path"

	"bitbucket.org/dbesen/scm/internal/filesystem"
	"bitbucket.org/dbesen/scm/internal/gob"
	"bitbucket.org/dbesen/scm/internal/hash"
)

type FolderHierarchy struct {
	FolderName  string
	FileMode    os.FileMode
	ChildHashes []string
}

type File struct {
	Filename    string
	ContentHash string
	// File mode/permissions aren't here because they're stored on the copy of the file in the db itself.
}

func StoreCWDNamed(name string) string {
	hash := StoreCWD()
	NameHash(name, hash)
	return hash
}

func StoreCWD() string {
	parentHash := GetCWDHash()
	currentHash := storePath("", ".")
	if !isChild(currentHash) {
		storeParentHash(currentHash, parentHash)
	}
	return currentHash
}

func RestoreCWDFromHashOrName(hashOrName string) {
	// Is it a hash or a name?
	hashOrName = GetHashForName(hashOrName)
	// Get & write the data with the hash
	restorePath(".", hashOrName)
}

func storeFileOnDisk(path string, fileInfoToRead os.FileInfo) string {
	h := hash.NewHasher()

	const BufferSize = 100
	file, err := os.Open(path_pkg.Join(path, fileInfoToRead.Name()))
	if err != nil {
		panic(err)
	}
	defer file.Close()

	buffer := make([]byte, BufferSize)

	for {
		bytesread, err := file.Read(buffer)

		if err != nil {
			if err != io.EOF {
				panic(err)
			}

			break
		}

		h.AddData(buffer[:bytesread])
	}

	hash := h.GetHash()

	// Store the actual content
	hash.EnsureHashFolder()

	f := File{
		Filename:    fileInfoToRead.Name(),
		ContentHash: hash.String(),
	}

	// TODO We're doing 2 stores here -- one for the copy and one to store the File struct.
	// That doubles the number of files.  Ideally we could store both in the same file, or
	// store all File structs in one file/a few files.  And all FolderHierarchy structs too?
	filesystem.CopyFile(path+"/"+f.Filename, path_pkg.Join(".scm", hash.Path(true)))

	gob.Register(File{})
	return gob.Store(f)
}

func storePath(path string, folderName string) string {
	fullPath := path_pkg.Join(path, folderName)

	children := make([]string, 0)

	info, err := os.Lstat(fullPath)
	if err != nil {
		panic(err)
	}
	fileMode := info.Mode()

	for _, file := range filesystem.GetFiles(fullPath) {
		var hash string
		if file.Name() == ".scm" {
			continue
		} else if Ignored(file.Name()) {
			continue
		} else if file.Mode().IsDir() {
			hash = storePath(fullPath, file.Name())
		} else {
			hash = storeFileOnDisk(fullPath, file)
		}
		children = append(children, string(hash))
	}

	folder := FolderHierarchy{
		FolderName:  folderName,
		FileMode:    fileMode,
		ChildHashes: children,
	}

	gob.Register(FolderHierarchy{})
	hash := gob.Store(folder)

	folderMode := os.FileMode(os.ModePerm)
	err = os.MkdirAll(".scm", folderMode)
	if err != nil {
		panic(err)
	}
	WriteHashFile(hash)

	return hash
}

func deleteAllExceptIgnored(path string) {
	if !filesystem.FolderExists(path) {
		return
	}

	for _, file := range filesystem.GetFiles(path) { // TODO the structure of this loop is dup'd

		fullPath := path_pkg.Join(path, file.Name())
		if file.Name() == ".scm" {
			continue
		} else if Ignored(file.Name()) {
			continue
		} else if file.Mode().IsDir() {
			deleteAllExceptIgnored(fullPath)
			filesystem.DeleteFolderIfEmpty(fullPath)
		} else {
			err := os.Remove(fullPath)
			if err != nil {
				panic(err)
			}
		}
	}
}

func restorePath(path string, hash string) {
	// Do the restore
	gob.Register(FolderHierarchy{})
	folder := gob.Get(hash).(FolderHierarchy)
	restorePathRecursive(path, folder)

	// Write out .scm/hash to track where we are in the tree
	WriteHashFile(hash)
}

func restorePathRecursive(path string, folder FolderHierarchy) {
	fullPath := path_pkg.Join(path, folder.FolderName)

	deleteAllExceptIgnored(fullPath)
	err := os.MkdirAll(fullPath, 0755)
	if err != nil {
		panic(err)
	}

	gob.Register(File{})
	gob.Register(FolderHierarchy{})

	for _, childHash := range folder.ChildHashes {
		raw := gob.Get(childHash)

		switch data := raw.(type) {
		case File:
			filesystem.CopyFile(
				path_pkg.Join(".scm", data.ContentHash),
				path_pkg.Join(fullPath, data.Filename),
			)
		case FolderHierarchy:
			restorePathRecursive(fullPath, data)
		}
	}

	// Now, set the user's mode on the folder
	err = os.Chmod(fullPath, folder.FileMode)
	if err != nil {
		panic(err)
	}
}
