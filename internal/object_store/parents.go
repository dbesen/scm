package object_store

import (
	"bitbucket.org/dbesen/scm/internal/filesystem"
)

var parentsMap map[string]string = LoadStringMapFromFile(".scm/sidecar/parents")

func GetParentHashOrName() string {
	return GetParentHashOrNameOf(GetCWDHash())
}

func GetParentHashOrNameOf(in string) string {
	hash, ok := parentsMap[GetHashForName(in)]
	if !ok {
		return "root"
	}
	return GetNameIfExists(hash)
}

func GetChildren() []string {
	return GetChildrenForHashOrName(GetCWDHash())
}

func GetChildrenForRoot() []string {
	// "root" may be in both parents and names.
	// In this case, we want to return the one from parents.
	return GetChildrenForName("root")
}

func GetChildrenForHashOrName(in string) []string {
	return GetChildrenForName(GetHashForName(in))
}

func GetChildrenForName(name string) []string {
	ret := make([]string, 0)
	for child, parent := range parentsMap {
		if parent == name {
			ret = append(ret, GetNameIfExists(child))
		}
	}
	return ret
}

func storeParentHash(child string, parent string) {
	if child == parent {
		panic(child + " parent of itself")
	}
	filesystem.EnsureFolder(".scm/sidecar")
	AppendKeyValueToFile(".scm/sidecar/parents", string(child), string(parent))
	parentsMap[child] = parent
}

func isChild(hash string) bool {
	for child := range parentsMap {
		if hash == child {
			return true
		}
		if hash == GetNameIfExists(child) {
			return true
		}
	}
	return false
}

func GetChildToParentMap() map[string]string {
	return parentsMap
}
