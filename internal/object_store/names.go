package object_store

var hashToName map[string]string = LoadStringMapFromFile(".scm/sidecar/names")

func reverseMap(m map[string]string) map[string]string {
	n := make(map[string]string, len(m))
	for k, v := range m {
		n[v] = k
	}
	return n
}

func GetHashForName(name string) string {
	nameToHash := reverseMap(hashToName)
	if val, ok := nameToHash[name]; ok {
		return val
	}
	return name
}

func GetNameIfExists(hash string) string {
	name, ok := hashToName[hash]
	if ok {
		return name
	}
	return hash
}

func NameHash(name string, hash string) {
	hashToName[hash] = name
	AppendKeyValueToFile(".scm/sidecar/names", hash, name)
}

func GetHashToNameMap() map[string]string {
	return hashToName
}
