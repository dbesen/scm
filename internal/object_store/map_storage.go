package object_store

import (
	"bufio"
	"os"

	"bitbucket.org/dbesen/scm/internal/filesystem"
	"bitbucket.org/dbesen/scm/internal/gob"
)

func LoadStringMapFromFile(filename string) map[string]string {
	// Returns an empty map if the file doesn't exist.
	gob.Register(map[string]string{})
	if !filesystem.FileExists(filename) {
		return make(map[string]string)
	}
	return gob.GetFromFilename(filename).(map[string]string)
}

func SaveStringMapToFile(filename string, data map[string]string) {
	gob.Register(map[string]string{})
	gob.StoreToFilename(filename, data)
}

func LoadSetFromFile(filename string) map[string]bool {
	// This function can't be converted to use gob because it has to be human-writable.

	// If the file doesn't exist, return empty
	ret := make(map[string]bool)
	if !filesystem.FileExists(filename) {
		return ret
	}
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		item := scanner.Text()
		ret[item] = true
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}
	return ret
}

func AppendKeyValueToFile(filename string, key string, value string) {
	data := LoadStringMapFromFile(filename)
	data[key] = value
	SaveStringMapToFile(filename, data)
}
