package object_store

type set map[string]bool

var ignoreList set = LoadSetFromFile(".scmignore")

func Ignored(name string) bool {
	if _, ok := ignoreList[name]; ok {
		return true
	}
	return false
}
