package object_store

import (
	"io/ioutil"
	"os"

	"bitbucket.org/dbesen/scm/internal/filesystem"
)

var cwdHash string = loadCWDHash()

func WriteHashFile(hash string) {
	mode := os.FileMode(0666)
	err := ioutil.WriteFile(".scm/hash", []byte(hash), mode)
	if err != nil {
		panic(err)
	}
	cwdHash = hash
}

func loadCWDHash() string {
	if !filesystem.FileExists(".scm/hash") {
		// No hash found, we're at root
		return "root"
	}
	// Get the hash
	hash, err := ioutil.ReadFile(".scm/hash")
	if err != nil {
		panic(err)
	}
	return string(hash)
}

func GetCWDHash() string {
	return cwdHash
}

func GetCWDHashOrName() string {
	return GetNameIfExists(cwdHash)
}
