package filesystem

import (
	"errors"
	"io"
	"os"
)

func EnsureFolder(folder string) {
	folderMode := os.FileMode(os.ModePerm)
	err := os.MkdirAll(folder, folderMode)
	if err != nil {
		panic(err)
	}

}

func CopyFile(from, to string) {
	source, err := os.Open(from)
	if err != nil {
		panic(err)
	}
	defer source.Close()

	destination, err := os.Create(to)
	if err != nil {
		panic(err)
	}
	defer destination.Close()

	_, err = io.Copy(destination, source)
	if err != nil {
		panic(err)
	}

	srcinfo, err := os.Lstat(from)
	if err != nil {
		panic(err)
	}

	err = os.Chmod(to, srcinfo.Mode())
	if err != nil {
		panic(err)
	}
}

func GetFiles(path string) []os.FileInfo {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	files, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		panic(err)
	}
	return files
}

func FileExists(filename string) bool {
	if _, err := os.Lstat(filename); err == nil {
		return true
	} else if errors.Is(err, os.ErrNotExist) {
		return false
	} else {
		panic(err)
	}
}

func FolderExists(foldername string) bool {
	if result, err := os.Lstat(foldername); err == nil {
		return result.IsDir()
	} else if errors.Is(err, os.ErrNotExist) {
		return false
	} else {
		panic(err)
	}
}

func DeleteFolderIfEmpty(path string) {
	if len(GetFiles(path)) > 0 {
		return
	}
	err := os.Remove(path)
	if err != nil {
		panic(err)
	}
}
