package hash

import (
	"encoding/base64"
	path_pkg "path"
	"strings"

	"bitbucket.org/dbesen/scm/internal/filesystem"
	"golang.org/x/crypto/sha3"
)

type Hash struct {
	hash_make  string
	hash_model string
	hash       []byte
}

func (h Hash) String() string {
	return h.Path(true)
}

func (h *Hash) Path(includeFilename bool) string {
	var sb strings.Builder
	sb.WriteString(h.hash_make)
	sb.WriteString("/")
	sb.WriteString(h.hash_model)
	sb.WriteString("/")
	if includeFilename {
		encoded := base64.URLEncoding.EncodeToString(h.hash)
		sb.WriteString(encoded)
	}
	return sb.String()
}

func (h *Hash) EnsureHashFolder() {
	filesystem.EnsureFolder(path_pkg.Join(".scm", h.Path(false)))
}

type Hasher struct {
	Shake sha3.ShakeHash
}

func NewHasher() *Hasher {
	var h Hasher
	h.Shake = sha3.NewShake128()
	return &h
}

func (h *Hasher) AddData(data []byte) {
	_, err := h.Shake.Write(data)
	if err != nil {
		panic(err)
	}
}

func (h *Hasher) GetHash() Hash {
	out := make([]byte, 32)
	_, err := h.Shake.Read(out)
	if err != nil {
		panic(err)
	}
	hash := Hash{
		hash_make:  "sha3",
		hash_model: "Shake128",
		hash:       out,
	}
	return hash
}

func SumData(data []byte) Hash {
	h := NewHasher()
	h.AddData(data)

	return h.GetHash()
}
