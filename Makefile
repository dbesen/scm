export RUN_TESTS_HASH := "163ff8cc663e884d98a3c9de5e073ffd52fdb635"
GO_FILES := $(shell find . -name '*.go')

.DEFAULT_GOAL := .tested

.tested: deps/run_tests.sh bin/scm tests/*
	go test ./...
	tests/test_suite.sh
	touch .tested

bin/scm: $(GO_FILES)
	go fmt ./...
	go build -o bin/scm -v ./cmd/scm

deps/run_tests.sh:
	mkdir -p deps
	wget -O deps/run_tests.sh https://bitbucket.org/dbesen/random/raw/$(RUN_TESTS_HASH)/tdd/bash/run_tests.sh

.PHONY: install-without-testing
install-without-testing: bin/scm
	go install -v ./cmd/scm

.PHONY: install
install: .tested install-without-testing

.PHONY: clean
clean:
	git clean -Xdff

.PHONY: would-clean
would-clean:
	git clean -Xdn
