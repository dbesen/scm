
SCM
===

Ways we're trying to be better than git
---------------------------------------
 - No concept of force push (but still allow for easy editing of the history tree?)
 - No concept of a staging area (but still support partial commits?)
 - No concept of "untracked" files; only ignored
 - Easily edit old commit messages
 - Blur the line between a monorepo & multiple repos
 - No concept of a "detached head" -- it's all just traversing the history tree
 - No concept of a stash -- it's all just traversing the history tree

What's different about scm
--------------------------
 - Should each commit have its own understanding of the entire history tree?
    - This would mean we can modify the tree without having to change older commits.  This avoids "force pushing", and it means we can "undo" a tree change by simply checking out an older commit.
    - But, it means when we ask scm what the history tree looks like, we have to tell it where we're looking from.  For example, `scm tree` shows the tree at the current commit.  `scm tree my-branch` shows `my-branch`'s understanding of the tree. 
    - If we did this, how would we move forward through history? The old tree doesn't know about new commits.

Notes
-----
 - Repo history is a tree.  We advance down a branch, making changes, then go back and start a new branch from a midpoint.
 - We do need to be able to combine branches.  So it's a DAG.

Open questions
--------------
 - Should we support "rewriting history" (i.e. deleting nodes, repositioning nodes)?

Use cases
---------
- Traverse the history tree
- Add nodes to the tree

TODO
----
1. If you save, create files/folders, then load, that should implicitly create an unnamed branch
1. Test 'scm' with no args
1. Test branch name should auto-advance on 'scm save' with no args
1. Test symbolic links
1. Allow naming of branches
1. scm/sidecar/parents isn't stored and should be
1. Compression
1. Performance
1. Clean command
1. Commit messages
1. Support version numbers?
1. Support copying data from another computer
1. Dogfood (move out of git)
1. Name project

LICENSE
-------
Copyright 2021 David K. Besen.  
No license is provided for this software.
