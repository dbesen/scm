#!/bin/bash

# TODO if this is run with no args, run all examples

start_folder=$(pwd)
mkdir running
cd running

function cleanup {
    cd "$start_folder"
    rm -rf running
}
trap cleanup EXIT

while read command; do
    [[ -z "$command" ]] && echo && continue # Blank lines
    [[ $command =~ ^#!.* ]] && continue # Shebang
    if [[ $command =~ ^#.* ]]; then
        # Comment
        echo "$command"
        continue
    fi
    echo -n "> "
    echo "$command"
    bash -c "$command"
done <"$start_folder/$@"
