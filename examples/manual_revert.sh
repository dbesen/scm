#!example_runner.sh

# First, let's make a few commits to work with:

echo "first contents" > file.txt
scm save a

echo "second contents" > file.txt
scm save b

echo "third contents" > file.txt
scm save c

scm show

# Now, we have three commits.  If we manually "revert" the file to the second commit, like so:

echo "second contents" > file.txt
scm save d

# Then, instead of making a new commit, scm moves to that commit in the tree, and renames it:

scm show
