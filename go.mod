module bitbucket.org/dbesen/scm

go 1.17

require (
	github.com/stretchr/testify v1.6.1
	golang.org/x/crypto v0.0.0-20211117183948-ae814b36b871
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20211123173158-ef496fb156ab // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
