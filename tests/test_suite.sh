#!/bin/bash -e

export PATH="../bin/:$PATH"

function setUpTest {
    start_folder=$(pwd)
    mkdir testing
    cd testing
}

function tearDownTest {
    cd $start_folder
    chmod -R u+w testing/
    rm -rf testing/
}

# Discover tests
for f in tests/*.sh; do
    if [ "$f" != "$0" ]; then
        . "$f"
    fi
done

# Run tests
. ./deps/run_tests.sh
