function test_name {
    echo "hi" > file.txt
    local HASH=`scm save`
    echo "bye" >> file.txt
    scm save second
    scm name first $HASH

    local OUTPUT="`scm show`"

    assertNotContains "sha3/" "$OUTPUT"
    assertContains "first" "$OUTPUT"
    assertContains "second" "$OUTPUT"
}

function test_name_no_args {
    assertContains Usage `scm name`
}

function test_name_one_arg {
    assertContains Usage `scm name asdf`
}

function test_name_with_spaces {
    echo "hi" > file.txt
    local HASH=`scm save`
    echo "bye" >> file.txt
    scm save "second name"
    scm name "first name" $HASH

    local OUTPUT="`scm show`"

    assertNotContains "sha3/" "$OUTPUT"
    assertContains "first name" "$OUTPUT"
    assertContains "second name" "$OUTPUT"
}