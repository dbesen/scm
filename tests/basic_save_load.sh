function test_save_load {
    echo "Hi" > file.txt
    scm save
    rm -f file.txt
    scm load
    assertFileContents "Hi" file.txt
}

function test_save_load_two_files {
    echo "Hi" > file.txt
    echo "Bye" > file2.txt
    scm save
    rm -f file.txt file2.txt
    scm load
    assertFileContents "Hi" file.txt
    assertFileContents "Bye" file2.txt
}

function test_file_in_folder {
    mkdir folder
    echo "Hi" > folder/file.txt
    scm save
    rm -rf folder
    scm load

    assertFileContents "Hi" folder/file.txt
}

function test_save_load_file_and_folder {
    mkdir folder
    echo "file in folder" > folder/file.txt
    echo "file outside folder" > file.txt
    scm save
    rm -f file.txt
    rm -rf folder/file.txt
    scm load

    assertFileContents "file in folder" folder/file.txt
    assertFileContents "file outside folder" file.txt
}

function test_doubly_nested_folder {
    mkdir -p folder1/folder2
    echo "Hi" > folder1/folder2/file.txt
    scm save
    rm -rf folder1
    scm load

    assertFileContents "Hi" folder1/folder2/file.txt
}

function test_empty_folder {
    mkdir folder
    scm save
    rmdir folder
    scm load

    # ls -la
    # ls -la folder
    assertFolderExists folder
}

function test_file_mode {
    echo "file" > file.txt
    chmod 456 file.txt
    scm save
    rm -f file.txt
    scm load

    assertFileMode 456 file.txt
}

function test_folder_mode {
    mkdir folder
    # Remember umask exists
    chmod 700 folder
    scm save
    rm -rf folder
    scm load
    assertFileMode 700 folder
}

function test_folder_mode_not_writable {
    mkdir folder
    chmod 400 folder
    scm save
    rm -rf folder
    scm load
    assertFileMode 400 folder
}

function test_folder_mode_not_writable_without_remove {
    mkdir folder
    chmod 400 folder
    scm save
    scm load
    assertFileMode 400 folder
}

function test_file_deleted_on_load {
    echo "Hi" > file.txt
    scm save
    echo "Bye" > delme.txt
    scm load
    assertFileNotExists delme.txt
}

function test_folder_deleted_on_load {
    echo "Hi" > file.txt
    scm save
    mkdir empty_folder
    scm load

    assertFolderNotExists empty_folder
}

function test_folder_deleted_on_load {
    echo "Hi" > file.txt
    scm save
    mkdir folder
    echo "Bye" > folder/delme.txt
    scm load
    assertNotFolder folder
    assertFileNotExists folder
}

function test_hidden_file_deleted_on_load {
    echo "Hi" > file.txt
    scm save
    echo "Bye" > .delme
    scm load
    assertFileNotExists .delme
}

function test_save_twice_load_once {
    echo "first" > file.txt
    scm save
    echo "second" > file.txt
    scm save
    rm file.txt # This is done by the load, but if scm was a noop then the test would pass without it.
    scm load

    assertFileContents "second" file.txt
}

function test_save_twice_load_older {
    echo "first" > file.txt
    local hash=`scm save`
    echo "second" > file.txt
    scm save
    scm load $hash

    assertFileContents "first" file.txt
}

function test_save_twice_same_hash {
    echo "Hi!" > file.txt
    local hash=`scm save`
    local hash2=`scm save`

    assertEquals $hash $hash2
}

