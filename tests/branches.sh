function test_move_branches {
    echo "Hi!" > file.txt
    local root=`scm save`

    echo "Branch 1" > file.txt
    local branch1=`scm save`

    scm load $root
    echo "Branch 2" > file.txt
    local branch2=`scm save`

    scm load $branch1
    assertFileContents "Branch 1" file.txt

    scm load $branch2
    assertFileContents "Branch 2" file.txt
}

function test_branching {
    echo "parent" > file.txt
    scm save parent
    echo "mybranch" > file.txt
    scm save mybranch
    scm load parent
    echo "mybranch2" > file.txt
    scm save mybranch2

    scm load mybranch
    assertFileContents "mybranch" file.txt
    scm load mybranch2
    assertFileContents "mybranch2" file.txt
}

function test_manual_revert {
    # Manual reverts should be treated the same as moving through the tree.
    echo "root" > file.txt
    scm save root
    echo "commit1" > file.txt
    scm save commit1
    echo "commit2" > file.txt
    scm save commit2
    echo "commit1" > file.txt
    scm save commit3

    assertEquals "root" `scm parent commit3` # Now it's called commit3, there is no longer a commit1
    assertEquals "commit3" `scm parent commit2`
    assertEquals "root" `scm parent` # Not commit2! Since we're at commit1 again, the parent is root
    assertNotContains "commit1" "`scm names`"
    assertNotContains "commit1" "`scm parents`"
}