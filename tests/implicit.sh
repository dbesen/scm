function test_implicit_save {
    echo "Hi!" > file.txt
    scm save root
    echo "Whatever" > file.txt
    scm load
    assertFileContents "Hi!" file.txt
    local hash=`scm children root` # there should only be 1 child, so only 1 line in the output
    scm load $hash
    assertFileContents "Whatever" file.txt
}

