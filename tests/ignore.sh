function test_ignore {
    echo "hi" > file.txt
    echo "bye" > file2.txt

    echo "file2.txt" > .scmignore
    scm save
    rm -f file.txt file2.txt
    scm load

    assertFileNotExists file2.txt
    assertFileContents "hi" file.txt
}

function test_ignore_folder {
    mkdir folder
    echo "hi" > folder/file.txt

    echo "folder" > .scmignore

    scm save
    rm -rf folder
    scm load

    assertNotFolder folder
    assertFileNotExists folder
}

function test_ignored_not_removed {
    echo "hi" > file.txt
    mkdir folder
    echo "bye" > folder/file.txt

    echo "file.txt" > .scmignore
    echo "folder" >> .scmignore

    scm save
    scm load

    assertFileContents "hi" file.txt
    assertFileContents "bye" folder/file.txt
}

