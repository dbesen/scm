function test_get_parent_no_history {
    assertEquals "root" `scm parent`
}

function test_get_parent_hash {
    echo "Parent" > file.txt
    scm save

    echo "Child" > file.txt
    scm save

    local parent_hash=`scm parent` # No args

    scm load $parent_hash
    assertFileContents "Parent" file.txt
}

function test_get_parent_of_name {
    echo "Parent" > file.txt
    scm save parent

    echo "Child" > file.txt
    scm save child

    local parent_name=`scm parent child`

    assertEquals "parent" $parent_name
}

function test_get_parent_of_parent {
    echo "Parent" > file.txt
    local parent=`scm save`

    echo "Child" > file.txt
    local child=`scm save`

    echo "Grandchild" > file.txt
    scm save

    local calculated_parent=`scm parent $child`

    assertEquals $parent $calculated_parent

    scm load $parent
    assertFileContents "Parent" file.txt
}

function test_get_parent_of_other_branch {
    echo "Parent" > file.txt
    local parent_hash=`scm save`

    echo "Branch 1" > file.txt
    local branch1parent=`scm save`

    echo "Branch 1 second" > file.txt
    local branch1child=`scm save`

    # Traverse to an unrelated branch
    scm load $parent_hash
    echo "Branch 2" > file.txt
    scm save

    local calculated_parent=`scm parent $branch1child`

    assertEquals $branch1parent $calculated_parent
    scm load $branch1parent

    assertFileContents "Branch 1" file.txt
}

function test_two_children_of_root {
    # This was a test case that actually failed
    echo "root" > file.txt
    local root_hash=`scm save`

    echo "branch 1" > file.txt
    local branch1_hash=`scm save`
    local branch1_parent=`scm parent`

    scm load $root_hash

    echo "branch 2" > file.txt
    local branch2_hash=`scm save`
    local branch2_parent=`scm parent`

    assertEquals $branch1_parent $root_hash
    assertEquals $branch1_parent $branch2_parent
}

