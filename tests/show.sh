# Show a tree of parents/children ala linux cli command 'tree'

function test_show {
    echo "hi" > file.txt
    scm save mycommit
    assertContains 'mycommit' "`scm show`"
}

function test_show_children {
    echo "hi" > file.txt
    scm save parent
    echo "hi2" >> file.txt
    scm save child
    assertContains 'parent' "`scm show`"
    assertContains 'child' "`scm show`"
}

function test_show_saves {
    echo "hi" > file.txt
    scm save parent
    echo "hi2" >> file.txt
    local OUTPUT=`scm show`

    assertContains "parent" "$OUTPUT"
    assertContains "sha3/" "$OUTPUT"
}

function test_show_no_root {
    scm save initial
    assertNotContains "root" "`scm show`"
}

function test_show_empty {
    assertContains "sha3/" "`scm show`"
}

function test_show_named_root {
    scm save root
    assertContains "root" "`scm show`"
}

function test_show_child_named_as_root {
    scm save empty
    echo "hi" > file.txt
    scm save first
    echo "bye" >> file.txt
    scm save root

    local OUTPUT=`scm show`
    assertContains "empty" "$OUTPUT"
    assertContains "first" "$OUTPUT"
    assertContains "root" "$OUTPUT"
}